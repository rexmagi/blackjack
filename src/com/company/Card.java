package com.company;

/**
 * Created by trae on 3/16/2016.
 */
public class Card {
    public String type;
    public char suit;
    public int[] value;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Card)) return false;

        Card card = (Card) o;

        return type.equals(card.type);

    }

    @Override
    public int hashCode() {
        return type.hashCode();
    }

    @Override
    public String toString() {
        return "{" +
                "suit=" + suit +
                ", type='" + type + '\'' +
                '}';
    }

    public Card(String type, int[] value, char suit) {
        this.type = type;
        this.value = value;
        this.suit = suit;
    }
}
