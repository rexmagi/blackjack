package com.company;

import java.util.ArrayList;

/**
 * Created by trae on 3/16/2016.
 */

public class Deck {
    public class EmptyDeckException extends Exception{}
    ArrayList<Card> deck;
    String []cards = {"A","2","3","4","5","6","7","8","9","10","J","Q","K"};
    int value[][] = {{1,11},{2},{3},{4},{5},{6},{7},{8},{9},{10},{10},{10},{10}};
    char []suits = {'C','D','H','S'};
    public Deck() {
       ShuffleDeck();
    }
    public void ShuffleDeck() {
        deck = new ArrayList();
        for (char s:suits) {
            for (int i = 0; i < cards.length; i++) {
                deck.add(new Card(cards[i],value[i],s));
            }

        }
    }
    public void deal(Player p) throws EmptyDeckException {
       if(deck.size()==0)
           throw new EmptyDeckException();
        ArrayList<Card> c = new ArrayList<>();
        c.add(deck.remove((int)(Math.random()*deck.size())));
        p.updateHand(c);
    }
    public void deal(Player p, int i) throws EmptyDeckException {
        if(deck.size()==0)
            throw new EmptyDeckException();
        ArrayList<Card> c = new ArrayList<>();
        c.add(deck.remove((int)(Math.random()*deck.size())-1));
        p.updateHand(c,i);
    }

}
