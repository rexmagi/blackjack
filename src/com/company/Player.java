package com.company;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by trae on 3/16/2016.
 */
public class Player {
    public ArrayList<ArrayList<Card>> hand;
    boolean Dealer;
    boolean Bust =false;
    int score;
    ArrayList<Boolean> stand;
    public Player(boolean deal) {
        this.hand = new ArrayList<>();
        this.hand.add(new ArrayList<>());
        this.Dealer = deal;
        this.stand = new ArrayList<>();
        stand.add(false);
    }
    public void updateHand(ArrayList<Card> c, int i ){
        hand.get(i).addAll(c);
    }
    public void updateHand(ArrayList<Card> c ) {
        Scanner eyes = new Scanner(System.in);
        hand.get(0).addAll(c);
        if(hand.get(0).size() == 2){
            if(hand.get(0).get(0).equals(hand.get(0).get(1)) && Dealer == false){
                System.out.print("Do You wish to Split (Y or N) >");
                if(eyes.next().equalsIgnoreCase("y"))
                    split();
            }
        }
    }
    public void emptyHand(){
        hand.clear();
        hand.add(new ArrayList<>());
        stand.clear();
        stand.add(false);
        Bust = false;
    }
    public int  Score(ArrayList<Card> h){
        int s = 0;
        for (Card c:h){
            if(c.value.length == 2 && s+c.value[1] <= 21  )
                s += c.value[1];
            else
                s += c.value[0];
        }
        return s;
    }
    public String  Score(){
        String score;
        int h1 = 0;
        int h2 = 0;
        if(hand.size() == 2){
            h1 = Score(hand.get(0));
            h2 = Score(hand.get(1));
            score = "{"+"h1="+Integer.toString(h1)+","+"h2="+Integer.toString(h2) +"}";
        }
        else{
            h1 = Score(hand.get(0));
            score = "{"+"h1="+Integer.toString(h1)+"}";
        }
        if(h1 > 21|| h2 >21 )
            Bust = true;
        if(h2>21 || (h1>h2 && h1 < 21))
            this.score = h1;
        else
            this.score = h2;

        return score;
    }
    public void split(){
        this.hand.add(new ArrayList<>());
        Card c = this.hand.get(0).remove(this.hand.size()-1);
        this.hand.get(1).add(c);
        stand.add(false);
    }
}
