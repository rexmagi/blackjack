package com.company;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner eyes = new Scanner(System.in);
        Deck d = new Deck();
        ArrayList<Player> players = new ArrayList<>();
        String choice = "y";
        boolean GameOver = false;
        int numGames=0,numPlayerWin=0,numDealerWin=0;
        int numPlayers = 1+1;
	    System.out.println("Black Jack Game\n==================");
        JOptionPane.showConfirmDialog(null,"How To Play"+"\n"+"\n"+
                "The Object Of The Game Is To Have More Points Than the Dealer Without Going Over 21"+"\n"+
                "A Tie Means The Dealer Wins"+"\n"+
                "Cards 2-10 Are Worth Their Number"+"\n"+
                "Jacks, Queens, And Kings Are Worth 10"+"\n"+
                "Aces are worth either 11 or 1"+"\n"+
                "You And The Dealer Are Dealt Two Cards"+"\n"+
                "You Will Know All of Your Points But Only You Can Only See One OF The Dealer's Cards Until His Turn"+"\n"+
                "You Can Then Choose To Hit or Stay"+"\n"+
                "If You Hit, You Will Be Given another card"+"\n"+
                "If You Stay, It Will Be The Dealer's Turn"+"\n"+
                "The Dealer Has To Hit Until He Has More Than 17 Points"+"\n"+
                "The Totals Are Then Compared To Decide The Winner");


        for (int i = 0; i < numPlayers; i++) {
            if(i == numPlayers - 1 )
                players.add(new Player(true));
            else
                players.add(new Player(false));
        }

        while(choice.equalsIgnoreCase("y")) {
            try {
                for (int i = 0; i < 2; i++) {
                    for (Player p : players) {
                        d.deal(p);
                    }
                }
                while(!GameOver ){
                    for (int i = 0; i < numPlayers ; i++) {
                        String p = (i != numPlayers - 1) ? Integer.toString(i + 1) : "Dealer";
                        System.out.println("Player " + p + " hand is " + players.get(i).hand);
                        System.out.println("Player " + p + " Score is " + players.get(i).Score());

                        if (i != numPlayers - 1) {
                            for (int j = 0; j < players.get(i).hand.size(); j++) {
                                if(players.get(i).stand.get(j) == true)
                                    continue;
                                System.out.print("Do you wish to Hit hand " + Integer.toString(j + 1) + "(Y or N) Player" + Integer.toString(i + 1) + "> ");
                                choice = eyes.next();
                                if (choice.equalsIgnoreCase("Y"))
                                    d.deal(players.get(i), j);
                                else{
                                    players.get(i).stand.set(j,true);
                                }
                            }
                            int y = 0;
                            for (int j = 0; j < players.get(i).stand.size(); j++) {
                                if(players.get(i).stand.get(j))
                                    y++;
                            }
                            if (y==players.get(i).stand.size() )
                                GameOver = true;
                        } else
                            if(players.get(i).score <= 17)
                            d.deal(players.get(numPlayers - 1));

                        System.out.println("Player " + p + " hand is " + players.get(i).hand);
                        System.out.println("Player " + p + " Score is " + players.get(i).Score());
                        if (players.get(i).Bust)
                            GameOver = true;
                    }
                }

            } catch (Exception e) {
                System.out.println("The deck is empty");
            }

            System.out.println("Game Over");
            if(players.get(0).score>players.get(1).score || players.get(1).Bust){
                numPlayerWin++;
                System.out.println("Player Won");
            } else{
                numDealerWin++;
                System.out.println("Dealer Won");}
            for (Player p : players) {
                p.emptyHand();
            }
            d.ShuffleDeck();
            numGames++;
            System.out.print("Do you wish to keep playing (Y or N)> ");
            choice = eyes.next();
            GameOver = false;
            if(choice.equalsIgnoreCase("y"))
                System.out.println("New Game\n================");
        }
        System.out.println();
        System.out.println("Statistics:");
        System.out.println("Number of Games Played: "+Integer.toString(numGames));
        System.out.println("Number of Player Won: "+Integer.toString(numPlayerWin));
        System.out.println("Number of Dealer Won: "+Integer.toString(numDealerWin));
        System.out.println("Player win percentage:  "+Double.toString((double)numPlayerWin/numGames));
    }
}
